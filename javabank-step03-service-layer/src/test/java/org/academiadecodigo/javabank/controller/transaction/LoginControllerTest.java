package org.academiadecodigo.javabank.controller.transaction;

import org.academiadecodigo.javabank.controller.Controller;
import org.academiadecodigo.javabank.controller.LoginController;
import org.academiadecodigo.javabank.services.AuthService;
import org.academiadecodigo.javabank.view.View;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;


public class LoginControllerTest {

// Testar o init(), ver se invoca o show()



    private View viewMock;
    private LoginController loginController;
    private AuthService authServiceMock;
    private Controller nextControllerMock;

    @Before
    public void setup(){
    loginController = new LoginController();
    viewMock = mock(View.class);
    authServiceMock = mock(AuthService.class);
    nextControllerMock = mock(Controller.class);
    loginController.setView(viewMock);
    loginController.setAuthService(authServiceMock);
    loginController.setNextController(nextControllerMock);

    }


    @Test
    public void voidinitTest(){
        loginController.init();
        verify(viewMock).show();
    }

    @Test
    public void onLoginSucessTest(){
        int fakeId = 999;
        when(authServiceMock.authenticate(fakeId)).thenReturn(true);
        // Invocação do método a testar
        loginController.onLogin(fakeId);

        verify(authServiceMock).authenticate(fakeId);
        verify(nextControllerMock).init();
        verify(viewMock, never()).show();
    }

    @Test
    public void setLoginFailTest(){
        int fakeId = 444;
        when(authServiceMock.authenticate(anyInt())).thenReturn(false);
        loginController.onLogin(fakeId);

        verify(authServiceMock).authenticate(fakeId);
        verify(viewMock).show();
    }






}