package org.academiadecodigo.javabank.controller.transaction;
import org.academiadecodigo.javabank.model.account.Account;
import org.academiadecodigo.javabank.services.AccountService;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

public class DepositControllerTest {

private AccountService accountServiceMock;
private DepositController depositControllerMock;


    @Before
    public void setup(){
        depositControllerMock = new DepositController();
        accountServiceMock = mock(AccountService.class);
        depositControllerMock.setAccountService(accountServiceMock);
    }

    @Test
    public void submitTransactionTest(){
        int accountId = anyInt();
        double amount = anyDouble();
        depositControllerMock.submitTransaction(accountId, amount);
        verify(accountServiceMock).deposit(accountId, amount);
    }

}