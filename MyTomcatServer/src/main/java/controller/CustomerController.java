package controller;

import model.Customer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomerController extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Customer customer = new Customer();
        customer.setName("Rui");

        request.setAttribute("firstCustomer", customer);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("src/webapp/WEB-INF/customer.jsp");
        dispatcher.forward(request, response);

    }
}
