import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.*;
@RunWith()
public class MyAddProjectTest {


    private MyAddProject myAddProject;

    @Before
    public void setup() {
        myAddProject = new MyAddProject();

    }

    // O que vamos testar? : receber 2 numeros e retornar a soma dos mesmos
    // se receber um 8 e um 4 deve retornar 12
    @Test
    public void sumTest() {
        int num1 = 8;
        int num2 = 4;
        int expected = 12;
        int result = myAddProject.sum(num1, num2);

        Assert.assertEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void sumNegativeTest() {
        myAddProject.sum(-8, 4);
    }


}


