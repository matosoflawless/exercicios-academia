package org.academiadecodigo.badashes;


import java.util.PriorityQueue;

public class TodoList{


    private TodoItem item;

    private PriorityQueue<TodoItem> priorityQueue;


    public TodoList () {
        priorityQueue = new PriorityQueue<>();

    }

    public void addItem(Importance importance,int priority, String description){
        this.item = new TodoItem(importance,description,priority);
        priorityQueue.add(item);

    }

    public boolean isEmpty(){
        if (priorityQueue.size() == 0) {
            return true;
        }
    return false;
    }

    public TodoItem remove(){
        return priorityQueue.remove();
    }



}
