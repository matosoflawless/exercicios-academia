package org.academiadecodigo.badashes;



public class TodoItem implements Comparable<TodoItem> {
    String discription;
    int priority;
    Importance importance;

    public TodoItem (Importance importance, String discription, int priority) {
        this.discription = discription;
        this.importance = importance;
        this.priority = priority;

    }




    @Override
    public int compareTo(TodoItem o) {
        return importance.compareTo(o.importance);
    }

    @Override
    public String toString() {
        return "TodoItem{" +
                "discription='" + discription + '\'' +
                ", priority=" + priority +
                ", importance=" + importance +
                '}';
    }
}
