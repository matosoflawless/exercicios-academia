import org.academiadecodigo.badashes.Importance;
import org.academiadecodigo.badashes.TodoItem;
import org.academiadecodigo.badashes.TodoList;

import java.util.PriorityQueue;

import static org.academiadecodigo.badashes.Importance.*;

public class Main {
    public static void main(String[] args) {
        TodoList todoList = new TodoList();

        todoList.addItem(Importance.MEDIUM, 3, "Medium priority 1");
        todoList.addItem(Importance.LOW, 1, "Low priority 1");
        todoList.addItem(HIGH, 3, "High priority 1");
        todoList.addItem(LOW, 2, "Low priority 2");
        todoList.addItem(MEDIUM, 2, "Medium priority 2");
        todoList.addItem(HIGH, 3, "High priority 2");

        System.out.println();
        todoList.isEmpty();

        while(!todoList.isEmpty()){
            System.out.println(todoList.remove());
        }

    }
}