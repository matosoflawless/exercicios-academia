package org.academiadecodigo.javabank.controller;

import org.academiadecodigo.javabank.persistence.dao.CustomerDao;
import org.academiadecodigo.javabank.persistence.model.Customer;
import org.academiadecodigo.javabank.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class CustomerListController {

    private CustomerService customerService;

    private CustomerDao customerDao;
    // Map the URL/Verb to the method
    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService= customerService;
    }

    @Autowired
    public void setCustomerDao(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/list")
    public String customerList(Model model) {

        List<Customer> customers = customerDao.findAll();

// Add an attribute to the request
        model.addAttribute("customers", customers);

// Return the view name
        return "list";
    }
}
