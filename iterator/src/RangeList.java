import java.util.Iterator;

public class RangeList implements Iterable<Integer> {

    private Iterator<Integer> iterator;

    private int min;

    private int max;

    public RangeList(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new RangeIterator();
    }


    private class RangeIterator implements Iterator<Integer> {

        int currentNumber = getMin();

        @Override
        public boolean hasNext() {
            if (currentNumber < getMax()) {
                return true;
            }
            return false;
        }

        @Override
        public Integer next() {

            return currentNumber++;
        }

    }

    }





