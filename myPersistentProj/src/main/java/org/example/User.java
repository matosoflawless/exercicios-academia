package org.example;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;


@javax.persistence.Entity
@Table(name = "Persistent_Test")

public class User {

        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        private Integer id;


        private String name;
        private Integer age;

        @Embedded
        private Adress adress;

        @Transient
        private String email;
        @Version
        private Integer version; // Permite utilizar o optimistic lock

        @CreationTimestamp
        private Date creationDate;







        //---------SETTERS----------------
        public void setId(Integer id) {
            this.id = id;
        }

        public void setName(String name) {
                this.name = name;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public void setAdress(Adress adress) {
                this.adress = adress;
        }



        //---------GETTERS----------------

        public Adress getAdress() {
                return adress;
        }

        public Integer getId() {
                return id;
        }

        public String getName() {
                return name;
        }

        public Integer getAge() {
                return age;
        }

        public String getEmail() {
                return email;
        }
}

