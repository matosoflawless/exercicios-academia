package org.example;



import jdk.jfr.Category;
import oneToOne.Car;
import oneToOne.Owner;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.Locale;

/* { Exercise }
Entity Mapping
Configure an entity class with optimistic lock and timestamps
use hibernate hbm2ddl to create the database schema
and persist and retrieve a few entity objects
 */

public class Main {
    public static void main(String[] args) {
        /*
        // Establish connection
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TEST");
        EntityManager entityManager = emf.createEntityManager();

        // Get the result
        System.out.println(entityManager.createNativeQuery("SELECT 1 + 1").getSingleResult());

        // Close connection
        entityManager.close();
        emf.close();
*/
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TEST"); // test -> o que foi definido no xml

        EntityManager em = emf.createEntityManager();

        /*
        User user = new User();
        user.setAdress(new Adress());




        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();

        Customer customer = new Customer();
        Account account = new Account();

       //customer
        customer.setName("Olinda");
        customer.setId(1);

       //account
*/
        /*
        account.setId(2);
        account.setBalance(33.00);
        em.getTransaction().begin();
        em.persist(customer);
        em.persist(account);
        em.getTransaction().commit();

        em.getTransaction().begin();
        Car car = new Car();
        car.setId(1);
        car.setGears(5);
        car.setMaxSpeed(169);
        em.persist(car);
        Boat boat = new Boat();
        boat.setEngines(8);
        boat.setMaxSpeed(300);
        boat.setId(2);
        em.persist(boat);
        em.getTransaction().commit();


*/
//EXERCICIO TABLE PER CLASS

   /*
    em.getTransaction().begin();
        Car c = new Car();
        Boat b = new Boat();
        c.setId(1);
        c.setMaxSpeed(200);
        c.setGears(5);
        b.setEngines(4);
        b.setId(2);
        b.setMaxSpeed(300);
        em.persist(b);
        em.persist(c);
    em.getTransaction().commit();

    */


        //JOINED TABLE
/*
        em.getTransaction().begin();
        Car carrinho = new Car();
        Boat barquinho = new Boat();
        carrinho.setGears(4);
        carrinho.setMaxSpeed(300);
        carrinho.setId(1);
        barquinho.setId(2);
        barquinho.setEngines(6);
        barquinho.setMaxSpeed(500);
        em.persist(barquinho);
        em.persist(carrinho);
        em.getTransaction().commit();
*/


//one to one

        /*
        em.getTransaction().begin();
        Car carrito_one = new Car();
        Owner ownzerzito_one = new Owner();
        carrito_one.setMake("Toyota");
        carrito_one.setModel("Trueno");

        carrito_one.setOwner(ownzerzito_one);
        em.persist(ownzerzito_one);
        em.persist(carrito_one);


        ownzerzito_one.setName("Joel");
        ownzerzito_one.setCar(carrito_one);
        em.getTransaction().commit();

*/

        em.getTransaction().begin();
        





        em.close();
        emf.close();




    }
}