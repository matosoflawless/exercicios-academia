package org.example.Mapped_SupperClass;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity

public class Account extends AbstractModel {
    public Double getBalance() {
        return balance;
    }


    //getters and setters

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    private Double balance;
}