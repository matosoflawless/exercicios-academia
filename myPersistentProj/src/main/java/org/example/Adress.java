package org.example;


import javax.persistence.Embeddable;

@Embeddable
public class Adress {

    private String city = "porto";
    private String street = "Rua Dr. Eduardo Santos";


    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

}
