package TablePerClass;

import SingleTableInheritance.Vehicle;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "car car car")
@Table(name = "CAR CAR CAR")
public class Car extends Vehicle {
    private Integer gears;

    public Integer getGears() {
        return gears;
    }

    public void setGears(Integer gears) {
        this.gears = gears;
    }
}