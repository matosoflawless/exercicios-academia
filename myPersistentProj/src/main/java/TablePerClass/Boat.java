package TablePerClass;

import SingleTableInheritance.Vehicle;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "boat boat boat")
@Table(name = "BOAT  BOAT BOAT")
public class Boat extends Vehicle {
    private Integer engines;

    public Integer getEngines() {
        return engines;
    }

    public void setEngines(Integer engines) {
        this.engines = engines;
    }
}

