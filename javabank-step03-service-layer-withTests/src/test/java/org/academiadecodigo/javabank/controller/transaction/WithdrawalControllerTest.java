package org.academiadecodigo.javabank.controller.transaction;
import org.academiadecodigo.javabank.services.AccountService;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

public class WithdrawalControllerTest {

    private AccountService accountServiceMock;
    private WithdrawalController withdrawalControllerMock;


    @Before
    public void setup(){
        withdrawalControllerMock = new WithdrawalController();
        accountServiceMock = mock(AccountService.class);
        withdrawalControllerMock.setAccountService(accountServiceMock);
    }

    @Test
    public void submitTransactionTest(){
        int accountId = anyInt();
        double amount = anyDouble();
        withdrawalControllerMock.submitTransaction(accountId, amount);
        verify(accountServiceMock).withdraw(accountId, amount);
    }


}