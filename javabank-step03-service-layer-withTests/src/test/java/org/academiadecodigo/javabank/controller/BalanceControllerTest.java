package org.academiadecodigo.javabank.controller;

import org.academiadecodigo.javabank.model.Customer;
import org.academiadecodigo.javabank.services.CustomerService;
import org.junit.Before;
import org.junit.Test;
import org.academiadecodigo.javabank.services.AuthService;
import org.academiadecodigo.javabank.view.View;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class BalanceControllerTest {


    private View viewMock;
    private BalanceController balanceController;
    private CustomerService customerServiceMock;
    private AuthService authServiceMock;
    private Customer customerMock;

    @Before
    public void setup(){
        balanceController = new BalanceController();

        viewMock = mock(View.class);
        authServiceMock = mock(AuthService.class);
        customerServiceMock = mock(CustomerService.class);
        customerMock = mock(Customer.class);

        balanceController.setView(viewMock);
        balanceController.setAuthService(authServiceMock);
        balanceController.setCustomerService(customerServiceMock);

    }
    

    @Test
    public void getCustomerTest() {
       balanceController.getCustomer();
       verify(authServiceMock).getAccessingCustomer();

    }

    @Test
    public void getCustomerBalance() {
        int fakeId = 999;
        double fakeBalance = 20.5;
        double DOUBLE_DELTA = 0.1; //Margem de erro
        when(authServiceMock.getAccessingCustomer()).thenReturn(customerMock);
        when(customerMock.getId()).thenReturn(fakeId);
        when(customerServiceMock.getBalance(fakeId)).thenReturn(fakeBalance);

        assertEquals(balanceController.getCustomerBalance(), fakeBalance, DOUBLE_DELTA);

    }
}