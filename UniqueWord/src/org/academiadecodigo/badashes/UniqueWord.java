package org.academiadecodigo.badashes;

import javax.print.DocFlavor;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class UniqueWord implements Iterable<String> {

    private Set<String> myList;
    private String word;
    private String[] myArray;

    public UniqueWord(String string)  {
        myList = new HashSet<>();
        myArray = string.split(" ");
        for (int i=0; i < myArray.length; i++) {
            myList.add(myArray[i]);
            //System.out.println(myList);
        }

    }



    @Override
    public Iterator<String> iterator() {

        return myList.iterator();
    }



}
