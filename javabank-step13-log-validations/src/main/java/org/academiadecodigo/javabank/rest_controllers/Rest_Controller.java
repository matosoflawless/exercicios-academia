package org.academiadecodigo.javabank.rest_controllers;

import org.academiadecodigo.javabank.command.AccountDto;
import org.academiadecodigo.javabank.command.CustomerDto;
import org.academiadecodigo.javabank.converters.AccountToAccountDto;
import org.academiadecodigo.javabank.converters.CustomerDtoToCustomer;
import org.academiadecodigo.javabank.converters.CustomerToCustomerDto;
import org.academiadecodigo.javabank.converters.RecipientToRecipientDto;
import org.academiadecodigo.javabank.persistence.dao.CustomerDao;
import org.academiadecodigo.javabank.persistence.model.Customer;
import org.academiadecodigo.javabank.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class Rest_Controller {

    private CustomerService customerService;

    private CustomerToCustomerDto customerToCustomerDto;
    private CustomerDtoToCustomer customerDtoToCustomer;
    private AccountToAccountDto accountToAccountDto;
    private RecipientToRecipientDto recipientToRecipientDto;
    private Customer customer;

    /**
     * Sets the customer service
     *
     * @param customerService the customer service to set
     */
    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     * Sets the converter for converting between customer model objects and customer form objects
     *
     * @param customerToCustomerDto the customer to customer form converter to set
     */
    @Autowired
    public void setCustomerToCustomerDto(CustomerToCustomerDto customerToCustomerDto) {
        this.customerToCustomerDto = customerToCustomerDto;
    }

    /**
     * Sets the converter for converting between customer form and customer model objects
     *
     * @param customerDtoToCustomer the customer form to customer converter to set
     */
    @Autowired
    public void setCustomerDtoToCustomer(CustomerDtoToCustomer customerDtoToCustomer) {
        this.customerDtoToCustomer = customerDtoToCustomer;
    }

    /**
     * Sets the converter for converting between account objects and account dto objects
     *
     * @param accountToAccountDto the account to account dto converter to set
     */
    @Autowired
    public void setAccountToAccountDto(AccountToAccountDto accountToAccountDto) {
        this.accountToAccountDto = accountToAccountDto;
    }

    /**
     * Sets the converter for converting between recipient model objects and recipient form objects
     *
     * @param recipientToRecipientDto the recipient to recipient form converter to set
     */
    @Autowired
    public void setRecipientToRecipientDto(RecipientToRecipientDto recipientToRecipientDto) {
        this.recipientToRecipientDto = recipientToRecipientDto;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     *
     * @return customerList
     */
  @RequestMapping(
          method = RequestMethod.GET,
          value = "/customer",
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<List<CustomerDto>> showCustomerList(){
      List<CustomerDto> customerList = customerToCustomerDto.convert(customerService.list());
      return new ResponseEntity<>(customerList, HttpStatus.OK);
  }



    /**
     *
     * @return id from specific customer
     */
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/customer/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<CustomerDto> showCustomerID(@PathVariable Integer id){
        CustomerDto customerDto = customerToCustomerDto.convert(customerService.get(id));
        return new ResponseEntity<>(customerDto, HttpStatus.OK);
    }


    /**
     *
     * @return Read all accounts for customer
     */
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/customer/{id}/accounts",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<AccountDto>> showCustomerAccounts(@PathVariable Integer id){
        List<AccountDto> accountDto = accountToAccountDto.convert(customerService.get(id).getAccounts());
        return new ResponseEntity<>(accountDto, HttpStatus.OK);
    }


    /**
     *
     * @return Read account for customer
     */
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/customer/{id}/account/{aid}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<AccountDto> showOneCustomerAccount(@PathVariable Integer id, @PathVariable Integer aid){
        AccountDto accountDto = accountToAccountDto.convert(customerService.get(id).getAccounts().get(aid));
        return new ResponseEntity<>(accountDto, HttpStatus.OK);
    }


    /**
     *
     * @return Create a new customer
     */
    @RequestMapping(
            method = RequestMethod.POST,
            value = "/customer",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<CustomerDto> createCustomer() {
        CustomerDto customerDto = customerToCustomerDto.convert(new Customer());
        return new ResponseEntity<>(customerDto, HttpStatus.OK);
    }


    /**
     *
     * @return Edit an existing customer by id
     */
    @RequestMapping(
            method = RequestMethod.PUT,
            value = "/customer/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<CustomerDto> editCustomer(@PathVariable Integer id) {
        CustomerDto customerDto = customerToCustomerDto.convert(customerService.save(customerService.get(id)));
        return new ResponseEntity<>(customerDto, HttpStatus.OK);
    }


/*
  @RequestMapping(
          method = RequestMethod.POST,
          value = "/customer",
          produces = MediaType.APPLICATION_JSON_VALUE
  )
    public ResponseEntity<CustomerDto> createCustomer(@RequestBody CustomerDto customerDto) {
        Customer savedCustomer = customerService.save(customerDtoToCustomer.convert(customerDto));
        CustomerDto savedCustomerDto = customerToCustomerDto.convert(savedCustomer);
        return new ResponseEntity<>(savedCustomerDto, HttpStatus.OK);
    }

*/

}
