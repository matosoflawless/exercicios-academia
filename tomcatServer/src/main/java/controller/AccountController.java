package controller;

import model.Account;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccountController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Account account = new Account();
        account.setId(69);
        account.setPersonName("lois");
        account.setBalance(-7000.00);

        req.setAttribute("account", account);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/account.jsp");
        dispatcher.forward(req, resp);

    }
}
