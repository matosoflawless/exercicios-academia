package controller;

import model.Customer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FormController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/form.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String info = req.getParameter("username");

        Customer currentCustomer = new Customer();
        currentCustomer.setName(info);
        currentCustomer.setEmail("matoso@matoso.com");
        currentCustomer.setPhoneNumber(911056598);

        req.getSession().setAttribute("customer", currentCustomer);

       resp.sendRedirect("http://localhost:8080/hello/customer");
    }
}
