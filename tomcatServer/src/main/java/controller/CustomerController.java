package controller;

import model.Customer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@Controller
public class CustomerController  {

    Customer customer = new Customer();

    @RequestMapping(method = RequestMethod.GET, value = "/customer")

    public String helloCustomer(Model model){

        customer.setName("Johnny the Sinner");
        customer.setEmail("touch_me_because_i_like_it");

        model.addAttribute("customer", customer);
        model.addAttribute("email", customer);

        return "customer";
    }



}
