package org.academiadecodigo.badasshes;

public interface Destroyable {

    void hit(int damage);

    boolean isDestroyed();
}
