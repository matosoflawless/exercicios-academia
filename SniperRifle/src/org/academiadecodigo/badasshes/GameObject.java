package org.academiadecodigo.badasshes;

abstract class GameObject {

    String objectName;


    public GameObject(String objectName) {
        this.objectName = objectName;
    }

    public String getMessage() {
        return objectName;
    }

}
