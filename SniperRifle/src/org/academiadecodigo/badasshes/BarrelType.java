package org.academiadecodigo.badasshes;

public enum BarrelType {

    PLASTIC("Plastic barrel",50),
    WOOD("Wooden barrel",100),
    METAL("Metallic barrel",150);



    private int maxDamage;
    private final String barrelType;


    private BarrelType (String barrelType, int maxDamage) {
        this.barrelType = barrelType;
        this.maxDamage = maxDamage;

    }

    public String getBarrelType() {
        return barrelType;
    }





    public int getMaxDamage() {
        return maxDamage;
    }

}
