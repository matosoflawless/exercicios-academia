package org.academiadecodigo.badasshes;

public class ArmouredEnemy extends Enemy implements Destroyable{
    private int armour = 100;


    public ArmouredEnemy() {
        super("Armoured Enemy",100);

    }
    @Override
    public void hit(int shoot){
    if (armour == 0) {
        super.setHealth(super.getHealth() - shoot);
    }
    if ((armour - shoot) <= 0) {
        shoot -= armour;
        armour = 0;
        super.setHealth((super.getHealth() - shoot));
    }
    else {
        armour -= shoot;
        System.out.println("Armour: " + armour);
    }
    }

}
