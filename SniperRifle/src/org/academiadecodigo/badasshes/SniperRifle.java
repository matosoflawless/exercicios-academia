package org.academiadecodigo.badasshes;

public class SniperRifle {

    private String name;
    private int bulletDamage;
    private int shotsFired = 0;
    private int treesNumber=0;
    private int enemiesNumber=0;
    private int barrelNumber=0;

    public SniperRifle(String name, int bulletDamage) {
        this.name = name;
        this.bulletDamage = bulletDamage;
    }
    public void shootTarget(Destroyable target){


        while(!target.isDestroyed()) {
            int randomShot = (int) (Math.random() * 100);

            if(randomShot<65){
                if(target instanceof Enemy) {
                    target.hit(bulletDamage);
                    System.out.println("Shots fired! Enemy shot!");
                    System.out.println("Health: " + ((Enemy) target).getHealth());
                    shotsFired++;
                }
                if(target instanceof Barrel){
                    target.hit(bulletDamage);
                    System.out.println("Shots fired! Barrel shot! Be CAREFUL!!");
                    System.out.println("Damage: " + ((Barrel)target).getMaxDamage());
                    shotsFired++;

                }
            }
            else {
                System.out.println("Missed shot!");
                shotsFired++;
            }
        }
    }
//
    public void aim(GameObject[] gameObject){
        for(int i=0; i < gameObject.length; i++){
            System.out.println("Type: " + gameObject[i].getMessage());
            if(gameObject[i] instanceof Destroyable){
                shootTarget((Destroyable)gameObject[i]);

//Condição para arrebentar o proximo target dentro do index.


                if(gameObject[i] instanceof Barrel && gameObject[i+1] instanceof Enemy && i < gameObject.length - 1){
                    ((Enemy)gameObject[i+1]).setDestroyed();
                    System.out.println("An Enemy went KABOOM too!!");
                    i++;
                }

                if(gameObject[i] instanceof Enemy) enemiesNumber++;
                if(gameObject[i] instanceof Barrel) barrelNumber++;



            }
            if(gameObject[i] instanceof Tree) {
            System.out.println("Dont shoot at the trees!! Save the planet!");
            treesNumber++;
            }
        }


    }

    public int getShotsFired(){
        return shotsFired;
    }

    public int getTreesNumber(){
        return treesNumber;
    }

    public int getEnemiesNumber(){
        return enemiesNumber;
    }

    public int getBarrelNumber(){
        return barrelNumber;
    }

}
