package org.academiadecodigo.badasshes;

abstract class Enemy extends GameObject implements Destroyable {
    private int health;
    private boolean isDead;
    private boolean isDestroyed;




    public Enemy(String objectName, int health){
        super(objectName);
        this.health = health;
    }
    public boolean isDestroyed(){
        return isDestroyed;
    }
    public boolean isDead(){
        return isDead;
    }


    public void hit(int shoot){
        if(health - shoot > 0) {
            health -= shoot;
        }
        else{
            health = 0;
            setDead();
        }
    }



    public int getHealth() {
        if (health <= 0) {
            setDead();
            return 0;
        }
        return health;
    }

    public void setHealth(int health) {
        this.health = health;

    }

    public void setDestroyed(){
        isDestroyed = true;
        setDead();
    }
    public void setDead() {
        isDead = true;
        isDestroyed = true;
        System.out.println("Target is down!!");
    }


}
