package org.academiadecodigo.badasshes;

public class Game {

    private int numOfObjects;
    private GameObject[] gameObjects;
    private SniperRifle sniperRifle;
    private int shotsFired;

    public void start() {

        SniperRifle sniperRifle = new SniperRifle("Mosin Nagant", 25);
        gameObjects = createObjects(numOfObjects);
        sniperRifle.aim(gameObjects);
        System.out.println("All targets down!! No trees shot!");
        System.out.println("Are you not entertained??");
        System.out.println("Shots fired: " + sniperRifle.getShotsFired());
        System.out.println("Total enemies: " + sniperRifle.getEnemiesNumber());
        System.out.println("Total trees: " + sniperRifle.getTreesNumber());
        System.out.println("Total barrels: " + sniperRifle.getBarrelNumber());

    }

    public GameObject[] createObjects(int numOfObjects) {
        this.numOfObjects = numOfObjects;
        gameObjects = new GameObject[numOfObjects];
        int randomChoice;
        for (int i = 0; i < gameObjects.length; i++) {
            randomChoice = (int) (Math.random() * 100);
            //creates trees
            if (randomChoice <= 8) {
                gameObjects[i] = new Tree();
            }
            //creates explosive barrels

            if (randomChoice <= 25 && randomChoice > 8) {

                switch (BarrelType.values()[(int) (Math.random() * BarrelType.values().length)]) {
                    case WOOD:
                        gameObjects[i] = new Barrel(BarrelType.WOOD.getBarrelType(), BarrelType.WOOD.getMaxDamage());
                    case METAL:
                        gameObjects[i] = new Barrel(BarrelType.METAL.getBarrelType(), BarrelType.METAL.getMaxDamage());
                    case PLASTIC:
                        gameObjects[i] = new Barrel(BarrelType.PLASTIC.getBarrelType(), BarrelType.PLASTIC.getMaxDamage());
                    default:
                        gameObjects[i] = new Barrel(BarrelType.PLASTIC.getBarrelType(), BarrelType.PLASTIC.getMaxDamage());
                }


            }
            //creates armoured enemies
            if (randomChoice <= 70 && randomChoice > 25) {
                gameObjects[i] = new ArmouredEnemy();
            }
            //creates normal enemies
            if (randomChoice <= 100 && randomChoice > 70) {
                gameObjects[i] = new SoldierEnemy();
            }
        }
        return gameObjects;
    }
}
