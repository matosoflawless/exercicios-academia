package org.academiadecodigo.badasshes;

public class Barrel extends GameObject implements Destroyable {

    private boolean isDestroyed;
    private int maxDamage;

    public Barrel(String barrelType,int maxDamage) {
        super(barrelType);
        this.maxDamage = maxDamage;
    }

    public void hit(int damage) {
        if (maxDamage - damage > 0) {
            maxDamage -= damage;


        } else {
            isDestroyed = true;
            System.out.println("KABOOOOOOOM!!!");
            System.out.println(super.getMessage() + " destroyed!!");
            maxDamage = 0;
        }

    }

    public int getMaxDamage(){
        return maxDamage;
    }

    public boolean isDestroyed() {
     return isDestroyed;
    }
}
