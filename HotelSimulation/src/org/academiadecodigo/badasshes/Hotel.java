package org.academiadecodigo.badasshes;

public class Hotel {
    private String hotelName;

    private Receptionist receptionist;

    private int roomsTotal;
    private int roomsAvailable;
    private People[] clients;

    //setters

    public Hotel (String hotelName, int roomsTotal, int roomsAvailable){
        this.hotelName = hotelName;
        this.roomsTotal = roomsTotal;
        this.roomsAvailable = roomsAvailable;

        //array de clientes
        this.clients = new People[roomsTotal];
        //instanciamento do rececionista
        receptionist = new Receptionist(this.roomsTotal, this.roomsAvailable, this.clients);
    }

    public void welcomingPeople(People p){
        if(receptionist.checkIn(p)){
            System.out.println("Welcome to " + hotelName + ", " + p.getName() + "!");
            System.out.println("You're checked-in!");
        }
        else{
            System.out.println("Maybe go easy on the drinks " + p.getName() + "!");
        }
    }

    public void goodbyeingPeople(People p) {
        if(receptionist.checkOut(p)) {
            System.out.println("You're checked-out!");
            System.out.println("Have a nice day, " + p.getName() + "! See you next time!");
        }
        else {
            System.out.println("Goodbye jovem " + p.getName() + "!");
        }
    }




}
