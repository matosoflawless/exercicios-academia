package org.academiadecodigo.badasshes;

public class Main {


    public static void main(String[] args) {
        Hotel sheraton = new Hotel("Sheraton",30,30);
        People costumer = new People("Otávio");
        sheraton.welcomingPeople(costumer);
        sheraton.goodbyeingPeople(costumer);
        Hotel maleiro = new Hotel("Maleiro",10,10);
        People costumer2 = new People("Jonny");
        maleiro.welcomingPeople(costumer2);
        maleiro.welcomingPeople(costumer2);
        sheraton.goodbyeingPeople(costumer2);
        System.out.println();
        System.out.println();
        People costumer3 = new People("Gonçalo");
        Hotel cesarsPalace = new Hotel("Cesar's Palace", 100, 0);
        cesarsPalace.welcomingPeople(costumer3);
        cesarsPalace.goodbyeingPeople(costumer3);


    }
}
