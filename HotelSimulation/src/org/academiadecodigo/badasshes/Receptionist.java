package org.academiadecodigo.badasshes;

public class Receptionist {

    private final int roomsTotal;
    private int roomsAvailable;

    private People[] clients;

    private boolean checkInStatus;

    public Receptionist(int roomsTotal, int roomsAvailable, People clients[]){
        this.roomsTotal = roomsTotal;
        this.roomsAvailable = roomsAvailable;
        this.clients = clients;
    }

    public boolean checkIn(People p){
        int positionAvailable = clients.length - 1;

        if(roomsAvailable>0){

            for(int i= 0; i < clients.length; i++){

                if(p.equals(clients[i])) {
                    System.out.println("Sorry " + p.getName() +  ", you already checked-in in this hotel!");
                    return false;
                }
                if(clients[i] == null){
                    positionAvailable = i;
                }

           }
           roomsAvailable--;
           clients[positionAvailable] = p;
           return true;
        }
        else{
            System.out.println("Sorry " + p.getName() + ", we got no rooms available!\nThere's more hotels down the road!");
            return false;
        }
    }

    public boolean checkOut(People p) {
        if (roomsAvailable <= roomsTotal) {

            for (int i = 0 ; i < clients.length; i++) {
                if (p.equals(clients[i])) {
                    clients[i] = null;
                    roomsAvailable++;
                    return true;
                }
            }
            System.out.println(p.getName() + ", you are not checked in at this hotel! So obviously you can't do a check-out!");
            System.out.println("Drink less my friend!");
            return false;
        }
        System.out.println("System error! Can't have more rooms available than the total");
        return false;
    }

}






