package org.academiadecodigo.javabank.dto;

import org.academiadecodigo.javabank.persistence.model.Customer;

public class CustomerConverter {

    public CustomerDTO converterToDto(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO();

        customerDTO.setFirst_Name(customer.getFirstName());
        customerDTO.setLast_Name(customer.getLastName());
        customerDTO.setEmail(customer.getEmail());
        customerDTO.setPhone(customer.getPhone());
        return customerDTO;
    }

    public Customer converterToCustomer(CustomerDTO customerDTO) {
        Customer customer = new Customer();

        customer.setFirstName(customerDTO.getFirst_Name());
        customer.setLastName(customerDTO.getLast_Name());
        customer.setEmail(customerDTO.getEmail());
        customer.setPhone(customerDTO.getPhone());
        return customer;
    }
}
