public class NotEnoughPermissionsException extends FileException {

    public NotEnoughPermissionsException(String message) {
        super("Not enough permissions, please login");
    }
}
