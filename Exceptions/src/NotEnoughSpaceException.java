public class NotEnoughSpaceException extends FileException{

public NotEnoughSpaceException(String message) {
    super("Not enough space");
}
}
