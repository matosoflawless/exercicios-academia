public class FileNotFoundException extends FileException {

    public FileNotFoundException(String message) {
        super("File not found");
    }
}
