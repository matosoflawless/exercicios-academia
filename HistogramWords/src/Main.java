import org.academiadecodigo.badashes.WordHistogram;
import org.academiadecodigo.badashes.WordHistogramComp;

public class Main {

    public static final String STRING = "test word words test test 1 10 1";
    public static void main(String[] args) {

        WordHistogramComp wordHistogramComp = new WordHistogramComp(STRING);

        System.out.println("Map has " + wordHistogramComp.size() + " distinct words");

        for (String word : wordHistogramComp) {
            System.out.println(word + " : " + wordHistogramComp.get(word));
        }

    }
}