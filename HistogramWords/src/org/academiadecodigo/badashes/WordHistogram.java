package org.academiadecodigo.badashes;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class WordHistogram extends HashMap<String,Integer> implements Iterable<String> {


    public String[] splittedWords;
    public Map<String,Integer> wordHistogram = new HashMap<>();

    public WordHistogram(String str) {
        splittedWords = str.split(" ");


        for (String word : splittedWords) {
            if (wordHistogram.containsKey(word)) {
                wordHistogram.put(word, wordHistogram.get(word) + 1);
            } else {
                wordHistogram.put(word, 1);
            }
        }
    }

     /*   public get(String word) {
            return wordHistogram.get(word);

    }

      */


    public int size() {
        return wordHistogram.size();
    }

    @Override
    public Iterator<String> iterator() {
        return (Iterator<String>) wordHistogram.keySet().iterator();
    }
}
