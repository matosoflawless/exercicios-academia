package org.academiadecodigo.badashes;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class WordHistogramComp implements Iterable<String> {
    public String[] splittedWords;
    public Map<String,Integer> wordHistogram = new HashMap<>();

    public WordHistogramComp(String str) {
        splittedWords = str.split(" ");


        for (String word : splittedWords) {
            if (wordHistogram.containsKey(word)) {
                wordHistogram.put(word, wordHistogram.get(word) + 1);
            } else {
                wordHistogram.put(word, 1);
            }
        }
    }

    public int get(String word) {
        return wordHistogram.get(word);
    }

    public int size() {
        return wordHistogram.size();
    }

    @Override
    public Iterator<String> iterator() {
        return wordHistogram.keySet().iterator();
    }

    // maneira de saber quantas palavras repetidas tem, no output tem que dar o numero de palavras que se repetem
    //




}
